# Dynamic Atlas

The Dynamic Atlas software is a web-based application designed to display geospatial data which changes over time.

This is a working demo which can be viewed at https://achale.gitlab.io/dynamicatlas/

This software is supported by a tutorial which can be accessed at https://achale.gitlab.io/dynamicatlastutorial/ and downloaded from https://gitlab.com/achale/dynamicatlastutorial/.

The Dynamic Atlas is a stand-alone application however if you would prefer to run it within a Shiny framework then see https://gitlab.com/achale/dynamicatlasshiny for further details.

Helper functions for configuring the Dynamic Atlas using Python are provided at https://gitlab.com/achale/dhaconfig however it would be advisable to follow the aforementioned tutorial and build a test website before attempting to use these functions.

