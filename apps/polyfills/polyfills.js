//
// Originally in map.js
//
// *** Polyfill *** to add compatible Object.keys support in older environments that don't natively support it; skimmed from developer.mozilla.org
if (!Object.keys) {
  Object.keys = (function() {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
        dontEnums = ['toString','toLocaleString','valueOf','hasOwnProperty','isPrototypeOf','propertyIsEnumerable','constructor'],
        dontEnumsLength = dontEnums.length;
    return function(obj) {
      if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) { throw new TypeError('Object.keys called on non-object') }
      var result = [], prop, i;
      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) { result.push(prop) }
      }
      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) { result.push(dontEnums[i]) }
        }
      }
      return result;
    };
  }());
}



//
// Originally in togglePortraitLandscape.js
//
// *** Polyfill *** to add support for indexOf() in older browsers; skimmed from developer.mozilla.org. Production steps of ECMA-262, Ed.5, 15.4.4.14
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {
    var k;
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }
    var o = Object(this);
    var len = o.length >>> 0;
    if (len === 0) { return -1 }
    var n = fromIndex | 0;
    if (n >= len) { return -1 }
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    while (k < len) {
      if (k in o && o[k] === searchElement) { return k }
      k++;
    }
    return -1;
  };
}
