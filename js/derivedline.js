
class Custom extends Chart.LineController {
  draw() {
    // Draw a vertical line at a given point in each dataset
    
    super.draw(arguments); // Call line controller method to plot all data

    var meta = this.getMeta(0);  // use zeroth dataset in datasets[{},{},{}]
    var pt0 = meta.data[this.chart.config._config.lineAtIndex.t];  // data relating to vertical line position as defined below in config = {...} 

    var {x} = pt0.getProps(['x']);
    var ctx = this.chart.ctx;
    var yscale = this.chart.scales.yAxes

    ctx.save();
    ctx.strokeStyle = this.chart.config._config.lineAtIndex.c
    ctx.lineWidth = 1
    ctx.beginPath();
    ctx.moveTo(x, yscale.bottom);
    ctx.lineTo(x, yscale.top);
    ctx.stroke();
    ctx.restore();
  }
}

Custom.id = 'derivedLine';
Custom.defaults = { lineWidth: 2 };   // Custom defaults. Line defaults are inherited.
Custom.overrides = Chart.overrides.line; // Overrides are only inherited, but not merged if defined
Chart.register(Custom); // Stores the controller

window.Custom = Custom // send to global environment

/*
const labels = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
];
const data = {
  labels: labels,
  datasets: [{
    label: 'My dataset',
    backgroundColor: 'rgb(255, 99, 132)',
    borderColor: 'rgb(255, 99, 132)',
    data: [0, 10, 5, 2, 20, 30, 45, 40],
  }]
};
const config = {
  type: 'derivedLine',
  data: data,
  options: {
    responsive: true,
    plugins: {
      title: {
        display: true,
        text: 'Derived Chart Type'
      },
    }
  },
  lineAtIndex: {t:3, c:'#0000FF'}  // position of line (3rd data point) and it's colour
};
var myChart = new Chart(
    document.getElementById('myChart').getContext('2d'),
    config
);

*/