//
// Dr Alison Hale, Lancaster University, UK is the sole author of this script togglePortraitLandscape.js which is published under the MIT License.
// See license.txt in root directory for details.
//


var fname1 = 'css/landscape.css',
    fname2 = 'css/portrait.css',
    fname0,
    refWidth = 1000, // width at which the landscape-portrait orientation switches
    wdw = window,
    inr = 'inner';


// *** select web page orientation *** There's no perfect way to do this but this seemed like the best comproise across PC and my mobile bowsers
function chooseOrientation(refWidth,wdw,inr) {
  //var f0 = (wdw[inr+'Width']>refWidth) ? fname1 : fname2; // originally only had this line of code in this function
  //alert("wdw[inr+'Width']="+wdw[inr+'Width']+"    refWidth="+refWidth + "  wdw[inr+'Height']="+wdw[inr+'Height'])
   var f0 = '',
       a = 1.3;
   if (wdw[inr+'Width']>refWidth) {
      f0 = fname1;      // landscape
      if ( (a*wdw[inr+'Width'])<wdw[inr+'Height'] ) { // give priority to landscape but then decide if this was correct choice based on factor 'a'
        f0 = fname2;    // portrait
      }
   } else {
      f0 = fname2;      // portrait
      if ( wdw[inr+'Width']>(a*wdw[inr+'Height']) ) { // give priority to portrait but then decide if this was correct choice based on factor 'a'
        f0 = fname1;    // landscape
      }
   }  
  return f0;
}


// *** if device changes orientation then toggle web page ***
//window.addEventListener("orientationchange", function() {  // Tested: seems ok but not perfect as portrait to landscape has zoom too large
// togglecss(fname0);
//}, false);


// *** if the window orientation has changed on resize then toggle web page ***
window.addEventListener("resize", function() {
  var f0 = chooseOrientation(refWidth,wdw,inr);
  if (f0 !== fname0) { togglecss(fname0) }  // only change orientation when needed
}, false);


// *** initially load either the landscape or portrait orientation CSS script ***
function initialloadcssjs(refWidth) {
  if ( !( 'innerWidth' in window ) ) {
    wdw = document.documentElement || document.body;
    inr = 'client';
  }
  fname0 = chooseOrientation(refWidth,wdw,inr);
  var filecss = document.createElement('link');
  filecss.rel = "stylesheet";
  filecss.type = "text/css";
  filecss.href = fname0;
  if (typeof filecss!=='undefined') { document.getElementsByTagName('head')[0].appendChild(filecss) }
}
initialloadcssjs(refWidth);


// *** toggle css styles depending on whether landscape or portrait orientation is requested ***
function togglecss(oldfname) {
  var updatedelements = 0,
      targetelement = 'link',
      targetattr = 'href',
      allexisting = document.getElementsByTagName(targetelement),
      newelement = document.createElement('link');
  fname0===fname1 ? fname0=fname2 : fname0=fname1;
  for (var i=allexisting.length; i>=0; i--) {            // search in reverse order for matching elements
    if (allexisting[i] && allexisting[i].getAttribute(targetattr)!==null && allexisting[i].getAttribute(targetattr).indexOf(oldfname)!==-1) {
      newelement.rel = "stylesheet";
      newelement.type = "text/css";
      newelement.href = fname0;
      allexisting[i].parentNode.replaceChild(newelement, allexisting[i]);
      updatedelements+=1;
    }
  }
}


// *** href to layer data which is avaiable for download; triggered from a hyperlink on the html page ***
function getData() {
  var radios = document.getElementsByName('layer'),
      drops = document.getElementById('control1dropdown');
  if (radios.length>0){                              // if radio buttons exist and a layer isn't selected then don't download data
      var checked = false;
      for (var i=0; i<radios.length; i++) { 
        if ( radios[i].checked===true ) { checked=true; break; }
      }
      if (checked===false) {
        alert("No data selected");
        return;
      }
  }
  if (drops!==null && drops.selectedIndex===0) {     // if dropdown exists and a layer isn't are selected then don't download data
    alert("No data selected");
    return;
  }
  if ( typeof currentLayer!=='undefined' && typeof layerdata[currentLayer.layer.options.i].downloadData!=='undefined' ){
    location.href = layerdata[currentLayer.layer.options.i].downloadData;
  }else{
    alert("Sorry, no data available");
  }
}
