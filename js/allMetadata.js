
addEventListener('load',
                 function(){
                     setTimeout(function(){
			 var ln;              // full layername
			 document.getElementById('loadholderbacking').style.display='none';
			 if (typeof layerId==='undefined') {
                             chooseLayer("Modelled-2021-06-01");
			 } else {
                             for ( var i=0; i<geodata.length; i++ ) {
				 ln = geodata[i].options.layerName;
				 if ( layerId===ln ) { 
				     map.addLayer(geodata[i]);
				     break;
				 }
                             }
			 }
                     }, 2000);
                 });

var radioNotDropdown = false;
var userDefinedBaseMaps = false;
var InitialMapCenterLatLng = [53.8731, -1.7718];
var InitialmapZoom = 6;
var mapUnits = true;
var secondMap = true;
var userDefinedBaseMaps = true;
var layerdata = [



// template
{
    filename: "data/modelled.geojson",
    layerName: ["Modelled-2021-06-01","Modelled-2021-06-07","Modelled-2021-06-14","Modelled-2021-06-21","Modelled-2021-06-28","Modelled-2021-07-05","Modelled-2021-07-12","Modelled-2021-07-19","Modelled-2021-07-26","Modelled-2021-08-02","Modelled-2021-08-09","Modelled-2021-08-16","Modelled-2021-08-23","Modelled-2021-08-30","Modelled-2021-09-06","Modelled-2021-09-13","Modelled-2021-09-20","Modelled-2021-09-27","Modelled-2021-10-04","Modelled-2021-10-11","Modelled-2021-10-18","Modelled-2021-10-25","Modelled-2021-11-01","Modelled-2021-11-08","Modelled-2021-11-15","Modelled-2021-11-22","Modelled-2021-11-29","Modelled-2021-12-06","Modelled-2021-12-13","Modelled-2021-12-20","Modelled-2021-12-27","Modelled-2022-01-03","Modelled-2022-01-10","Modelled-2022-01-17","Modelled-2022-01-24","Modelled-2022-01-31","Modelled-2022-02-07","Modelled-2022-02-14","Modelled-2022-02-21","Modelled-2022-02-28","Modelled-2022-03-07","Modelled-2022-03-14","Modelled-2022-03-21","Modelled-2022-03-28"], // prev_weekly
    geojsonname: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"], //prev_weekindex
    geojsongeom: "LAD21NM", // LAD21NM
    geojsonExtraInfo: "LAD21CD", //LAD21CD
    friendlyname: "LTLA Output", // "Wastewater gc/l 2021"
    mapPosition: {
        centerLatLng: [54, 0.7718],
        zoom: 8
    },
    regionNames: {
        country: "England",
        area: "LTLA"
    },
    colgrades: {
        Ints: 2,
        Inis: 0,
        Num: undefined,
        Vals: ["#FFF5F0","#FEE0D2","#FCBBA1","#FC9272","#FB6A4A","#EF3B2C","#CB181D","#99000D"]
    },
    legend: true,
    sliderlabel: "",
    mapStyles: {
            weight: 1,
            opacity: 1,
            color: "#aaa",
            fillOpacity: 0.9,
            smoothFactor: 1,
            radius: undefined
        },
        noDataColour: "rgba(0, 0, 0, 0.3)",
        featurehltStyle: {
            weight: 5
        },
        timeData: {
            CIname: "upper/lower CI",
            timeseries: true,
            timeseriesMin: 0,
            timeseriesMax: 43,
            timeseriesStep: 1,
            highlight: undefined,
            xlabs: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"]
        },
    meandata: [6.3499,6.3221,6.5271,7.385,8.6979,8.665,9.7479,10.1514,9.9455,9.5862,9.6088,10.0404,9.7093,9.6507,9.1067,8.6606,8.6662,7.9226,8.1541,7.9148,7.8367,8.2723,8.1964,9.1488,8.1431,8.1349,8.3835,8.8788,8.9622,9.2356,9.7935,9.4636,8.7544,8.7919,8.8424,8.4363,8.494,7.9771,7.621,7.9119,8.6941,9.0653,9.8884,9.4021],
        radioButtonValue: "Modelled-2021-06-01",
        timePlot: {
            Background1Colour: "rgba(0,0,255,0)",
            Line1Colour: "rgba(0,0,255,0.2)",
            Background2Colour: "#007ac3",
            Line2Colour: "#007ac3",
            MarkerSize: 0,
            HighlightColour: "#b41019",
            HighlightSize: 5,
            ymax: undefined
        },
        subsetOfLayers: undefined,
        layerMarker: undefined,
        mapBoundary: {
            filename: "data/border.geojson",
            borderStyle: {
                color: "#aaa",
                weight: 2
            }
        },
        units: {
            html: "log gc/l",
            unicode: "log gc/l",
            xlab: "week beginning"
        },
        downloadData: "data/modelled.geojson",
        popupBox: false
// template
}

    ,


// template
{
    filename: "data/regionmodel.geojson",
    layerName: ["Region-2021-06-01","Region-2021-06-07","Region-2021-06-14","Region-2021-06-21","Region-2021-06-28","Region-2021-07-05","Region-2021-07-12","Region-2021-07-19","Region-2021-07-26","Region-2021-08-02","Region-2021-08-09","Region-2021-08-16","Region-2021-08-23","Region-2021-08-30","Region-2021-09-06","Region-2021-09-13","Region-2021-09-20","Region-2021-09-27","Region-2021-10-04","Region-2021-10-11","Region-2021-10-18","Region-2021-10-25","Region-2021-11-01","Region-2021-11-08","Region-2021-11-15","Region-2021-11-22","Region-2021-11-29","Region-2021-12-06","Region-2021-12-13","Region-2021-12-20","Region-2021-12-27","Region-2022-01-03","Region-2022-01-10","Region-2022-01-17","Region-2022-01-24","Region-2022-01-31","Region-2022-02-07","Region-2022-02-14","Region-2022-02-21","Region-2022-02-28","Region-2022-03-07","Region-2022-03-14","Region-2022-03-21","Region-2022-03-28"], // prev_weekly
    geojsonname: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"], //prev_weekindex
    geojsongeom: "RGN21NM", // LAD21NM
    geojsonExtraInfo: "RGN21CD", //LAD21CD
    friendlyname: "Region Output", // "Wastewater gc/l 2021"
    mapPosition: {
        centerLatLng: [54, 0.7718],
        zoom: 8
    },
    regionNames: {
        country: "England",
        area: "Region"
    },
    colgrades: {
        Ints: 2,
        Inis: 0,
        Num: undefined,
        Vals: ["#FEE5D9","#FCBBA1","#FC9272","#FB6A4A","#EF3B2C","#CB181D","#99000D"]
    },
    legend: true,
    sliderlabel: "",
    mapStyles: {
            weight: 1,
            opacity: 1,
            color: "#aaa",
            fillOpacity: 0.9,
            smoothFactor: 1,
            radius: undefined
        },
        noDataColour: "rgba(0, 0, 0, 0.3)",
        featurehltStyle: {
            weight: 5
        },
        timeData: {
            CIname: "upper/lower CI",
            timeseries: true,
            timeseriesMin: 0,
            timeseriesMax: 43,
            timeseriesStep: 1,
            highlight: undefined,
            xlabs: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"]
        },
    meandata: [6.3499,6.3221,6.5271,7.385,8.6979,8.665,9.7479,10.1514,9.9455,9.5862,9.6088,10.0404,9.7093,9.6507,9.1067,8.6606,8.6662,7.9226,8.1541,7.9148,7.8367,8.2723,8.1964,9.1488,8.1431,8.1349,8.3835,8.8788,8.9622,9.2356,9.7935,9.4636,8.7544,8.7919,8.8424,8.4363,8.494,7.9771,7.621,7.9119,8.6941,9.0653,9.8884,9.4021],
        radioButtonValue: "Region-2021-06-01",
        timePlot: {
            Background1Colour: "rgba(0,0,255,0)",
            Line1Colour: "rgba(0,0,255,0.2)",
            Background2Colour: "#007ac3",
            Line2Colour: "#007ac3",
            MarkerSize: 0,
            HighlightColour: "#b41019",
            HighlightSize: 5,
            ymax: undefined
        },
        subsetOfLayers: undefined,
        layerMarker: undefined,
        mapBoundary: {
            filename: "data/border.geojson",
            borderStyle: {
                color: "#aaa",
                weight: 2
            }
        },
        units: {
            html: "log gc/l",
            unicode: "log gc/l",
            xlab: "week beginning"
        },
        downloadData: "data/regionmodel.geojson",
        popupBox: false
// template
}

    ,


// template
{
    filename: "data/ccgmodel.geojson",
    layerName: ["CCG-2021-06-01","CCG-2021-06-07","CCG-2021-06-14","CCG-2021-06-21","CCG-2021-06-28","CCG-2021-07-05","CCG-2021-07-12","CCG-2021-07-19","CCG-2021-07-26","CCG-2021-08-02","CCG-2021-08-09","CCG-2021-08-16","CCG-2021-08-23","CCG-2021-08-30","CCG-2021-09-06","CCG-2021-09-13","CCG-2021-09-20","CCG-2021-09-27","CCG-2021-10-04","CCG-2021-10-11","CCG-2021-10-18","CCG-2021-10-25","CCG-2021-11-01","CCG-2021-11-08","CCG-2021-11-15","CCG-2021-11-22","CCG-2021-11-29","CCG-2021-12-06","CCG-2021-12-13","CCG-2021-12-20","CCG-2021-12-27","CCG-2022-01-03","CCG-2022-01-10","CCG-2022-01-17","CCG-2022-01-24","CCG-2022-01-31","CCG-2022-02-07","CCG-2022-02-14","CCG-2022-02-21","CCG-2022-02-28","CCG-2022-03-07","CCG-2022-03-14","CCG-2022-03-21","CCG-2022-03-28"], // prev_weekly
    geojsonname: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"], //prev_weekindex
    geojsongeom: "CCG21NM", // LAD21NM
    geojsonExtraInfo: "CCG21CD", //LAD21CD
    friendlyname: "CCG Output", // "Wastewater gc/l 2021"
    mapPosition: {
        centerLatLng: [54, 0.7718],
        zoom: 8
    },
    regionNames: {
        country: "England",
        area: "CCG"
    },
    colgrades: {
        Ints: 2,
        Inis: 0,
        Num: undefined,
        Vals: ["#FEE5D9","#FCBBA1","#FC9272","#FB6A4A","#EF3B2C","#CB181D","#99000D"]
    },
    legend: true,
    sliderlabel: "",
    mapStyles: {
            weight: 1,
            opacity: 1,
            color: "#aaa",
            fillOpacity: 0.9,
            smoothFactor: 1,
            radius: undefined
        },
        noDataColour: "rgba(0, 0, 0, 0.3)",
        featurehltStyle: {
            weight: 5
        },
        timeData: {
            CIname: "upper/lower CI",
            timeseries: true,
            timeseriesMin: 0,
            timeseriesMax: 43,
            timeseriesStep: 1,
            highlight: undefined,
            xlabs: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"]
        },
    meandata: [6.3499,6.3221,6.5271,7.385,8.6979,8.665,9.7479,10.1514,9.9455,9.5862,9.6088,10.0404,9.7093,9.6507,9.1067,8.6606,8.6662,7.9226,8.1541,7.9148,7.8367,8.2723,8.1964,9.1488,8.1431,8.1349,8.3835,8.8788,8.9622,9.2356,9.7935,9.4636,8.7544,8.7919,8.8424,8.4363,8.494,7.9771,7.621,7.9119,8.6941,9.0653,9.8884,9.4021],
        radioButtonValue: "CCG-2021-06-01",
        timePlot: {
            Background1Colour: "rgba(0,0,255,0)",
            Line1Colour: "rgba(0,0,255,0.2)",
            Background2Colour: "#007ac3",
            Line2Colour: "#007ac3",
            MarkerSize: 0,
            HighlightColour: "#b41019",
            HighlightSize: 5,
            ymax: undefined
        },
        subsetOfLayers: undefined,
        layerMarker: undefined,
        mapBoundary: {
            filename: "data/border.geojson",
            borderStyle: {
                color: "#aaa",
                weight: 2
            }
        },
        units: {
            html: "log gc/l",
            unicode: "log gc/l",
            xlab: "week beginning"
        },
        downloadData: "data/ccgmodel.geojson",
        popupBox: false
// template
}

    ,


// template
{
    filename: "data/prevalence.geojson",
    layerName: ["DBPrev-2020-05-31","DBPrev-2020-06-07","DBPrev-2020-06-14","DBPrev-2020-06-21","DBPrev-2020-06-28","DBPrev-2020-07-05","DBPrev-2020-07-12","DBPrev-2020-07-19","DBPrev-2020-07-26","DBPrev-2020-08-02","DBPrev-2020-08-09","DBPrev-2020-08-16","DBPrev-2020-08-23","DBPrev-2020-08-30","DBPrev-2020-09-06","DBPrev-2020-09-13","DBPrev-2020-09-20","DBPrev-2020-09-27","DBPrev-2020-10-04","DBPrev-2020-10-11","DBPrev-2020-10-18","DBPrev-2020-10-25","DBPrev-2020-11-01","DBPrev-2020-11-08","DBPrev-2020-11-15","DBPrev-2020-11-22","DBPrev-2020-11-29","DBPrev-2020-12-06","DBPrev-2020-12-13","DBPrev-2020-12-20","DBPrev-2020-12-27","DBPrev-2021-01-03","DBPrev-2021-01-10","DBPrev-2021-01-17","DBPrev-2021-01-24","DBPrev-2021-01-31","DBPrev-2021-02-07","DBPrev-2021-02-14","DBPrev-2021-02-21","DBPrev-2021-02-28","DBPrev-2021-03-07","DBPrev-2021-03-14","DBPrev-2021-03-21","DBPrev-2021-03-28","DBPrev-2021-04-04","DBPrev-2021-04-11","DBPrev-2021-04-18","DBPrev-2021-04-25","DBPrev-2021-05-02","DBPrev-2021-05-09","DBPrev-2021-05-16","DBPrev-2021-05-23","DBPrev-2021-05-30","DBPrev-2021-06-06","DBPrev-2021-06-13","DBPrev-2021-06-20","DBPrev-2021-06-27","DBPrev-2021-07-04","DBPrev-2021-07-11","DBPrev-2021-07-18","DBPrev-2021-07-25","DBPrev-2021-08-01","DBPrev-2021-08-08","DBPrev-2021-08-15","DBPrev-2021-08-22","DBPrev-2021-08-29","DBPrev-2021-09-05","DBPrev-2021-09-12","DBPrev-2021-09-19","DBPrev-2021-09-26","DBPrev-2021-10-03","DBPrev-2021-10-10","DBPrev-2021-10-17","DBPrev-2021-10-24","DBPrev-2021-10-31","DBPrev-2021-11-07","DBPrev-2021-11-14","DBPrev-2021-11-21","DBPrev-2021-11-28","DBPrev-2021-12-05","DBPrev-2021-12-12","DBPrev-2021-12-19","DBPrev-2021-12-26","DBPrev-2022-01-02","DBPrev-2022-01-09","DBPrev-2022-01-16","DBPrev-2022-01-23","DBPrev-2022-01-30","DBPrev-2022-02-06","DBPrev-2022-02-13","DBPrev-2022-02-20","DBPrev-2022-02-27","DBPrev-2022-03-06","DBPrev-2022-03-13","DBPrev-2022-03-20","DBPrev-2022-03-27"], // prev_weekly
    geojsonname: ["2020-05-31","2020-06-07","2020-06-14","2020-06-21","2020-06-28","2020-07-05","2020-07-12","2020-07-19","2020-07-26","2020-08-02","2020-08-09","2020-08-16","2020-08-23","2020-08-30","2020-09-06","2020-09-13","2020-09-20","2020-09-27","2020-10-04","2020-10-11","2020-10-18","2020-10-25","2020-11-01","2020-11-08","2020-11-15","2020-11-22","2020-11-29","2020-12-06","2020-12-13","2020-12-20","2020-12-27","2021-01-03","2021-01-10","2021-01-17","2021-01-24","2021-01-31","2021-02-07","2021-02-14","2021-02-21","2021-02-28","2021-03-07","2021-03-14","2021-03-21","2021-03-28","2021-04-04","2021-04-11","2021-04-18","2021-04-25","2021-05-02","2021-05-09","2021-05-16","2021-05-23","2021-05-30","2021-06-06","2021-06-13","2021-06-20","2021-06-27","2021-07-04","2021-07-11","2021-07-18","2021-07-25","2021-08-01","2021-08-08","2021-08-15","2021-08-22","2021-08-29","2021-09-05","2021-09-12","2021-09-19","2021-09-26","2021-10-03","2021-10-10","2021-10-17","2021-10-24","2021-10-31","2021-11-07","2021-11-14","2021-11-21","2021-11-28","2021-12-05","2021-12-12","2021-12-19","2021-12-26","2022-01-02","2022-01-09","2022-01-16","2022-01-23","2022-01-30","2022-02-06","2022-02-13","2022-02-20","2022-02-27","2022-03-06","2022-03-13","2022-03-20","2022-03-27"], //prev_weekindex
    geojsongeom: "LAD21NM", // LAD21NM
    geojsonExtraInfo: "LAD21CD", //LAD21CD
    friendlyname: "Debiased Prevalence", // "Wastewater gc/l 2021"
    mapPosition: {
        centerLatLng: [54, 0.7718],
        zoom: 8
    },
    regionNames: {
        country: "England",
        area: "LTLA"
    },
    colgrades: {
        Ints: 2,
        Inis: -12,
        Num: undefined,
        Vals: ["#FEE5D9","#FCBBA1","#FC9272","#FB6A4A","#EF3B2C","#CB181D","#99000D"]
    },
    legend: true,
    sliderlabel: "",
    mapStyles: {
            weight: 1,
            opacity: 1,
            color: "#aaa",
            fillOpacity: 0.9,
            smoothFactor: 1,
            radius: undefined
        },
        noDataColour: "rgba(0, 0, 0, 0.3)",
        featurehltStyle: {
            weight: 5
        },
        timeData: {
            CIname: "upper/lower CI",
            timeseries: true,
            timeseriesMin: 0,
            timeseriesMax: 95,
            timeseriesStep: 1,
            highlight: undefined,
            xlabs: ["2020-05-31","2020-06-07","2020-06-14","2020-06-21","2020-06-28","2020-07-05","2020-07-12","2020-07-19","2020-07-26","2020-08-02","2020-08-09","2020-08-16","2020-08-23","2020-08-30","2020-09-06","2020-09-13","2020-09-20","2020-09-27","2020-10-04","2020-10-11","2020-10-18","2020-10-25","2020-11-01","2020-11-08","2020-11-15","2020-11-22","2020-11-29","2020-12-06","2020-12-13","2020-12-20","2020-12-27","2021-01-03","2021-01-10","2021-01-17","2021-01-24","2021-01-31","2021-02-07","2021-02-14","2021-02-21","2021-02-28","2021-03-07","2021-03-14","2021-03-21","2021-03-28","2021-04-04","2021-04-11","2021-04-18","2021-04-25","2021-05-02","2021-05-09","2021-05-16","2021-05-23","2021-05-30","2021-06-06","2021-06-13","2021-06-20","2021-06-27","2021-07-04","2021-07-11","2021-07-18","2021-07-25","2021-08-01","2021-08-08","2021-08-15","2021-08-22","2021-08-29","2021-09-05","2021-09-12","2021-09-19","2021-09-26","2021-10-03","2021-10-10","2021-10-17","2021-10-24","2021-10-31","2021-11-07","2021-11-14","2021-11-21","2021-11-28","2021-12-05","2021-12-12","2021-12-19","2021-12-26","2022-01-02","2022-01-09","2022-01-16","2022-01-23","2022-01-30","2022-02-06","2022-02-13","2022-02-20","2022-02-27","2022-03-06","2022-03-13","2022-03-20","2022-03-27"]
        },
    meandata: [-6.0638,-6.3975,-6.4828,-6.6168,-6.9611,-7.1402,-7.2653,-7.2436,-7.175,-7.2537,-7.0591,-7.039,-6.9374,-6.5263,-6.1448,-6.1288,-5.6165,-5.2648,-4.8679,-4.7315,-4.5495,-4.3602,-4.4182,-4.4164,-4.5668,-4.8504,-4.9234,-4.8762,-4.616,-4.4601,-4.0001,-4.0214,-4.205,-4.2453,-4.5223,-4.8203,-5.1494,-5.3365,-5.4651,-5.8677,-6.0298,-6.1353,-6.1933,-6.2821,-6.496,-6.6953,-6.9048,-7.0147,-7.0714,-7.1053,-7.1372,-6.9998,-6.5527,-6.1529,-5.9039,-5.6269,-5.1572,-4.8519,-4.554,-4.3332,-4.7216,-4.7375,-4.6735,-4.6248,-4.6289,-4.6686,-4.6148,-4.8589,-4.7458,-4.628,-4.5388,-4.3311,-4.1666,-4.2072,-4.3275,-4.4119,-4.3395,-4.3449,-4.3672,-4.3092,-3.9785,-3.4684,-2.9145,-2.7323,-3.0949,-3.2845,-3.2362,-3.2431,-3.3645,-3.5269,-3.5533,-3.5813,-3.1161,-2.8073,-2.6183,-2.5568],
        radioButtonValue: "DBPrev-2020-05-31",
        timePlot: {
            Background1Colour: "rgba(0,0,255,0)",
            Line1Colour: "rgba(0,0,255,0.2)",
            Background2Colour: "#007ac3",
            Line2Colour: "#007ac3",
            MarkerSize: 0,
            HighlightColour: "#b41019",
            HighlightSize: 5,
            ymax: undefined
        },
        subsetOfLayers: undefined,
        layerMarker: undefined,
        mapBoundary: {
            filename: "data/border.geojson",
            borderStyle: {
                color: "#aaa",
                weight: 2
            }
        },
        units: {
            html: "logit prevalence",
            unicode: "logit prevalence",
            xlab: "week beginning"
        },
        downloadData: "data/prevalence.geojson",
        popupBox: false
// template
}

    ,


   { 
       filename: 'data/exceed.geojson',
       friendlyname: '"Latest" Alert status',
       radioButtonValue: 'exceed-7',
       geojsonname: ["7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12"],
       layerName: ["exceed-7","exceed-7.5","exceed-8","exceed-8.5","exceed-9","exceed-9.5","exceed-10","exceed-10.5","exceed-11","exceed-11.5","exceed-12"], //['Prob log gc/l > thresh on 2022-03-28'],
       geojsongeom: 'LAD21NM',
       geojsonExtraInfo: 'LAD21CD',
       mapPosition: {centerLatLng:[55.5, -2.7], zoom:5.5},
       regionNames: {country:'0.5', area:'LAD name'},
       colgrades: {legtitle:'Pr(gc &#62; thresh)', Ints:0.1, Inis:0, Num:undefined, Vals:['#5e4fa2', '#66c2a5FF', '#20202040', '#fdae61FF', '#d7191cFF'], userDefined:[0, 0.03, 0.05, 0.95, 0.97, 1]},
       legend: true,
       sliderlabel: '',
       radius: 10,
       mapStyles: {weight:0, opacity:0.1, color:'#00000000', fillOpacity:0.8, smoothFactor:1, radius:6},
       noDataColour: 'rgba(0, 0, 0, 0.3)',
       featurehltStyle: {weight:3, color:'#000'},
       timeData: {timeseries:true, xlabs: [7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12], CIname:' ', highlight:undefined, timeseriesMin:0, timeseriesMax:10, timeseriesStep:1},
       meandata: [0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5],
       timePlot: {Background1Colour:'rgba(0,0,255,0)', Line1Colour:'rgba(0,0,255,.1)', Background2Colour:'#007ac3', Line2Colour:'#007ac3', Background3Colour:undefined, Line3Colour:undefined, MarkerSize:0, HighlightColour:'#b41019', HighlightSize:5, ymax:1, beginYAtZero:true},
       subsetOfLayers:undefined,
       layerMarker: undefined,
       mapBoundary: undefined,
       units: {html:'P(log gc/l > thresh)', unicode:'P(log gc/l > thresh)', xlab:'Threshold value on 2022-03-28'},
       downloadData: 'data/exceed.geojson',
       popupBox: false
   },
   
// rise2weeks
{ 
    filename: 'data/rise2weeks.geojson',
    friendlyname: 'Prob of 2-week rise',
    radioButtonValue: "rise-2021-06-14",
    geojsonname: ["2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"],
    layerName:["rise-2021-06-14","rise-2021-06-21","rise-2021-06-28","rise-2021-07-05","rise-2021-07-12","rise-2021-07-19","rise-2021-07-26","rise-2021-08-02","rise-2021-08-09","rise-2021-08-16","rise-2021-08-23","rise-2021-08-30","rise-2021-09-06","rise-2021-09-13","rise-2021-09-20","rise-2021-09-27","rise-2021-10-04","rise-2021-10-11","rise-2021-10-18","rise-2021-10-25","rise-2021-11-01","rise-2021-11-08","rise-2021-11-15","rise-2021-11-22","rise-2021-11-29","rise-2021-12-06","rise-2021-12-13","rise-2021-12-20","rise-2021-12-27","rise-2022-01-03","rise-2022-01-10","rise-2022-01-17","rise-2022-01-24","rise-2022-01-31","rise-2022-02-07","rise-2022-02-14","rise-2022-02-21","rise-2022-02-28","rise-2022-03-07","rise-2022-03-14","rise-2022-03-21","rise-2022-03-28"] ,
    geojsongeom: 'LAD21NM',
    geojsonExtraInfo: 'LAD21CD',
    mapPosition: {centerLatLng:[55.5, -2.7], zoom:5.5},
    regionNames: {country:'0.8 threshold', area:'LAD name'},
    colgrades: {
	legtitle:'Pr(2 week increase)',
	Ints:0.1, Inis:0,
	Num:undefined,
	Vals:['#40404040', 'yellow', 'orange', 'red'],
	userDefined:[0, 0.80, 0.90, 0.95, 1]},
    legend: true,
    sliderlabel: '',
    radius: 10,
    mapStyles: {weight:1, opacity:0.5, color:'black', fillOpacity:0.8, smoothFactor:1, radius:6},
    noDataColour: 'rgba(0, 0, 0, 0.3)',
    featurehltStyle: {weight:3, color:'#000'},
       timeData:{
	   timeseries: true,
	   timeseriesMin: 0,
	   timeseriesMax: 41,
	   timeseriesStep: 1,
	   highlight: "red",
	   xlabs: ["2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"]
       },
    meandata: [0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8],
    timePlot: {
	Background1Colour:'rgba(0,0,255,0)', Line1Colour:'rgba(255,0,0,.4)', Background2Colour:'#007ac3', Line2Colour:'rgba(0,0,255,.5)', Background3Colour:undefined, Line3Colour:undefined, MarkerSize:0, HighlightColour:'#b41019', HighlightSize:5, ymax:1, beginYAtZero:true},
    subsetOfLayers:undefined,
    layerMarker: undefined,
    mapBoundary: undefined,
    units: {html:'Prob of 2-week increase', unicode:'Prob of 2-week increase', xlab:'week midpoint'},
    downloadData: 'data/rise2weeks.geojson',
    popupBox: false
},


   
   // ww data

   {
       filename: "data/ww_data.geojson",
       layerName: ["Data-2021-06-01","Data-2021-06-07","Data-2021-06-14","Data-2021-06-21","Data-2021-06-28","Data-2021-07-05","Data-2021-07-12","Data-2021-07-19","Data-2021-07-26","Data-2021-08-02","Data-2021-08-09","Data-2021-08-16","Data-2021-08-23","Data-2021-08-30","Data-2021-09-06","Data-2021-09-13","Data-2021-09-20","Data-2021-09-27","Data-2021-10-04","Data-2021-10-11","Data-2021-10-18","Data-2021-10-25","Data-2021-11-01","Data-2021-11-08","Data-2021-11-15","Data-2021-11-22","Data-2021-11-29","Data-2021-12-06","Data-2021-12-13","Data-2021-12-20","Data-2021-12-27","Data-2022-01-03","Data-2022-01-10","Data-2022-01-17","Data-2022-01-24","Data-2022-01-31","Data-2022-02-07","Data-2022-02-14","Data-2022-02-21","Data-2022-02-28","Data-2022-03-07","Data-2022-03-14","Data-2022-03-21","Data-2022-03-28"],
       geojsonname: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"] ,

       geojsongeom: "uwwName", // LAD21NM
       geojsonExtraInfo: "uwwCode", //LAD21CD
       friendlyname: "WW Data", // "Wastewater gc/l 2021"
       mapPosition: {
           centerLatLng: [54, 0.7718],
           zoom: 8
       },
       regionNames: {
	   country: "England",
           area: "WWtW"
       },
       colgrades: {
           Ints: 2,
           Inis: 0,
           Num: undefined,
           Vals: ["#F7FCF5","#E5F5E0","#C7E9C0","#A1D99B","#74C476","#41AB5D","#238B45","#005A32"]
       },

       legend: true,
       sliderlabel: "",
       radius: 8,
       mapStyles: {
           weight: 1,
           opacity: 1,
           color: "#aaa",
           fillOpacity: 0.9,
           smoothFactor: 1,
           radius: undefined
       },
       noDataColour: "rgba(0, 0, 0, 0.3)",
       featurehltStyle: {
           weight: 5
       },

       timeData:{
	   timeseries: true,
	   timeseriesMin: 0,
	   timeseriesMax: 43,
	   timeseriesStep: 1,
	   highlight: undefined,
	   xlabs: ["2021-06-01","2021-06-07","2021-06-14","2021-06-21","2021-06-28","2021-07-05","2021-07-12","2021-07-19","2021-07-26","2021-08-02","2021-08-09","2021-08-16","2021-08-23","2021-08-30","2021-09-06","2021-09-13","2021-09-20","2021-09-27","2021-10-04","2021-10-11","2021-10-18","2021-10-25","2021-11-01","2021-11-08","2021-11-15","2021-11-22","2021-11-29","2021-12-06","2021-12-13","2021-12-20","2021-12-27","2022-01-03","2022-01-10","2022-01-17","2022-01-24","2022-01-31","2022-02-07","2022-02-14","2022-02-21","2022-02-28","2022-03-07","2022-03-14","2022-03-21","2022-03-28"]
       },
       meandata: [5.8124,5.807,6.037,6.8706,8.2259,8.1982,9.3778,9.8165,9.6206,9.2987,9.3486,9.7984,9.444,9.4093,8.8554,8.3543,8.3748,7.6292,7.9175,7.6898,7.6384,8.0682,7.9731,8.9535,7.8957,7.9288,8.1578,8.5877,8.6399,8.9197,9.56,9.2695,8.5375,8.598,8.6803,8.2566,8.3091,7.7851,7.4771,7.7744,8.5905,8.9705,9.7787,9.1932],
       timePlot: {
           Background1Colour: "rgba(0,0,255,0)",
           Line1Colour: "rgba(0,0,255,0.2)",
           Background2Colour: "#007ac3",
           Line2Colour: "#007ac3",
           MarkerSize: 0,
           HighlightColour: "#b41019",
           HighlightSize: 5,
           ymax: undefined
       },
       radioButtonValue: "Data-2021-06-01",
       subsetOfLayers: undefined,
       layerMarker: undefined,
       mapBoundary: {
           filename: "data/border.geojson",
           borderStyle: {
               color: "#aaa",
                weight: 2
            }
        },
        units: {
            html: "log gc/l",
            unicode: "log gc/l",
            xlab: "week beginning"
        },
        downloadData: "data/ww_data.geojson",
       popupBox: false
   },
]
